package com.trainingmicroservicemaul.wallet;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WalletApplication {

	public static void main(String[] args) {
		SpringApplication.run(WalletApplication.class, args);
	}
}
@EnableWebSecurity
class KonfigurasiResourceServer extends WebSecurityConfigurerAdapter {

	@Value("${security.oauth2.resourceserver.jwt.jwk-set-uri}")
	private String jwkSetUri;

	protected void configure(HttpSecurity http) throws Exception {
		http
				.authorizeRequests()
				.anyRequest().authenticated()
				.and()
				.oauth2ResourceServer()
				.jwt()
				.jwkSetUri(jwkSetUri);
	}
}

@RestController
class WalletController {

	@PreAuthorize("hasAuthority('SCOPE_review_transaksi')")
	@GetMapping("/wallet")
	public Map<String, Object> userWallet(Authentication authentication) {
		Map<String, Object> walletInfo = new HashMap<>();
		walletInfo.put("Current User", authentication.getPrincipal());
		walletInfo.put("Permissions", authentication.getAuthorities());
		return walletInfo;
	}
}

